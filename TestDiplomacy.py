#!/usr/bin/env python3


# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import input_parser, diplomacy_eval
from cut_support import cut_supp

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):

	# ----
	# Reading Suite
	# ----


	def test_read_1(self):
		move1 = """A Madrid Hold""" # convoy? eyes emoji
		m1 = input_parser(move1)
		self.assertEqual(m1[0], {'army': 'A', 'city' : "Madrid", 'action' : "h"})

	def test_read_2(self):
		move2 = """B Barcelona Move Madrid"""
		m2 = input_parser(move2)
		self.assertEqual(m2[0], {'army': 'B', 'city' : "Barcelona", 'action' : "m", 'target': "Madrid"})

	def test_read_3(self):
		move3 = """C London Support B"""
		m3 = input_parser(move3)
		self.assertEqual(m3[0], {'army': 'C', 'city' : "London", 'action' : "s", 'target': "B"})

	# -----
	# Diplomacy Results (w/ diplomacy_eval)
	# -----
	

	def test_diplomacy_1(self):
		moves1 = """A Madrid Hold
B Barcelona Move Madrid
C London Support B"""
		self.assertEqual(diplomacy_eval(moves1),["A [dead]","B Madrid","C London"])

	def test_diplomacy_2(self):
		moves2 = """A Madrid Hold
B Barcelona Move Madrid"""
		self.assertEqual(diplomacy_eval(moves2),["A [dead]","B [dead]"])


	def test_diplomacy_3(self):
		moves3 = """A Madrid Hold
B Barcelona Move Madrid
C London Move Madrid"""
		self.assertEqual(diplomacy_eval(moves3),["A [dead]","B [dead]","C [dead]"])


	def test_diplomacy_4(self):
		moves4 = """A Madrid Hold
B Barcelona Move Madrid
C London Support B
D Austin Move London"""
		self.assertEqual(diplomacy_eval(moves4),["A [dead]","B [dead]","C [dead]", "D [dead]"])


	def test_diplomacy_5(self):
		moves5 = """A Madrid Hold
B Barcelona Move Madrid
C London Support B
D Paris Support B"""
		self.assertEqual(diplomacy_eval(moves5),["A [dead]","B Madrid","C London", "D Paris"])


	def test_diplomacy_6(self):
		moves6 = """A Madrid Hold
B Barcelona Move Madrid
C London Support B
D Paris Support B
E Austin Support A"""
		self.assertEqual(diplomacy_eval(moves6),["A [dead]","B Madrid","C London", "D Paris","E Austin"])


	def test_diplomacy_7(self):
		moves7 = """A Madrid Hold
B Barcelona Move Madrid
C London Support B
D Paris Support B
E Austin Support A
F Helsinki Support A"""
		self.assertEqual(diplomacy_eval(moves7),["A [dead]","B [dead]","C London", "D Paris","E Austin","F Helsinki"])


	def test_diplomacy_8(self):
		moves8 = """A Madrid Hold
B Barcelona Move Madrid
C London Support B
D Paris Support B
E Austin Support A
F Helsinki Support A
G Oslo Move Helsinki
H Holzingen Support A"""
		self.assertEqual(diplomacy_eval(moves8),["A [dead]","B [dead]","C London", "D Paris","E Austin","F [dead]", "G [dead]", "H Holzingen"])


# ----
# cut support
# ----
	def test_cuts(self):
		cuts = cut_supp([{'army': 'A', 'city' : "Madrid", 'action' : "h"},{'army': 'B', 'city' : "Barcelona", 'action' : "m", 'target' : "Madrid"},{'army': 'C', 'city' : "London", 'action' : "s", 'target' : "A"},{'army': 'D', 'city' : "Berlin", 'action' : "m", 'target' : "London"}])
		answer = [{'army': 'A', 'city' : "Madrid", 'action' : "h"},{'army': 'B', 'city' : "Barcelona", 'action' : "m", 'target' : "Madrid"},{'army': 'C', 'city' : "London", 'action' : "h", 'final' : "London", 'target' : "A"},{'army': 'D', 'city' : "Berlin", 'action' : "m", 'target' : "London"}]
		self.assertEqual(cuts,answer)
# TODO

# ----
# main
# ----


if __name__ == "__main__":
	main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
