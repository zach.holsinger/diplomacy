def cut_supp(moves):

	# read ['target'] for individual letters
	# find places with attackers and supps in same city	
		# any['final'] == i['city'] && len(i['final']) == 1
	# change ['target'] from supp dest. to city 

	fights = []
	supps = []
	for i in moves:
		if i['action'] == 'm':
			fights.append(i['target']) 
		elif i['action'] == 's':
			supps.append(i)

	for i in supps:
		if i['city'] in fights:
			i['final'] = i['city']
			i['action'] = 'h'
			
	return moves