from cut_support import cut_supp

def input_parser(raw_commands):
	moves = []
	for i in str(raw_commands).split("\n"):
		moves.append(army_parser(i))
	return moves

def army_parser(raw_command):
	raw = raw_command.split()
	betteraction = raw[2].replace("Move", 'm').replace("Hold",'h').replace("Support",'s')
	dict_command = {'army' : raw[0], 'city' : raw[1], 'action' : betteraction}
	if len(raw) == 4:
		dict_command['target'] = raw[3]

	return dict_command

def diplomacy_eval(raw_commands):
	moves = input_parser(raw_commands)
	# get intentions
	for i in moves:

		i['supp'] = 0 # set support to 0

		if i['action'] == 'm': # add final destinations
			i['final'] = i['target'] 
		elif i['action'] == 'h':
			i['final'] = i['city']
			i['target'] = i['city']
		else:
			i['final'] = i['target']

	cut_supp(moves) # fix armies that are trying to support but are under attack
		# read ['target'] for individual letters
		# find places with attackers and supps in same city	
			# any['final'] == i['city'] && len(i['final']) == 1
		# change ['target'] from supp dest. to city 

	for supporter in moves:
		if len(supporter['final']) == 1:
			# find the remaining supports
			# add to the support tag 
			for attacker in moves:
				if attacker['army'] == supporter['final']:
					if attacker['supp'] == 0:
						attacker['supp'] = 1
					else:
						attacker['supp'] += 1
				supporter['resolution'] = supporter['city']
				supporter['target'] = 'supp'

	battles = {} # this function turns all the PLACES where battles will happen into keys. the values are the armies that are fighting
	for i in moves:
		if not len(i['final']) == 1 :
			if i['final'] not in battles :
				battles[i['final']] = i['army']
			else: 
				battles[i['final']] += i['army']

	for i in battles: # for each city that has a battle 
		maxsupp = 0
		winner = ""
		for j in battles[i]:
			# go along each army string (the entry in the battle dict that has the armies involved in an attack on a city)
			for k in moves:
				# print(k['supp'])
				# if the army has the most supports, then it wins and is named
				# if tie, maxsupp doesn't change but winner turns back to none
				if k['army'] == j:
					if  k['supp'] > maxsupp:
						maxsupp = k['supp']
						winner = k['army']
					elif k['supp'] == maxsupp:
						winner = ""
		for j in battles[i]:
			for k in moves:
				if k['army'] == j and k['army'] != winner and k['target'] != 'supp':
					k['resolution'] = "[dead]" #the armies that we NOT the winner, who WERE involved, and who were NOT supporters
				elif k['army'] == winner:
					# print(str(k['city']) + " invades " + str(k['target']))
					k['resolution'] = k['target']

		battles[i] = winner
		
	for b in moves: # for the rest of the armies that had no war, set their resolition to whatevre city they were in
		if 'resolution' not in b.keys():					#pragma: no cover
			b['resolution'] = b['city']			#pragma: no cover

	resolved = []
	for m in moves:
		# print(m['army'] + " " + m['resolution'])
		resolved.append(str(m['army']) + " " + str(m['resolution']))
	return resolved

def diplomacy_write(resolved, writer):			#pragma: no cover
	for i in resolved:							#pragma: no cover
		writer.write(i)								#pragma: no cover

def diplomacy_solve(read, write):
	r = diplomacy_eval(read)					#pragma: no cover
	diplomacy_write(r, write)					#pragma: no cover

